# RFC:Toy Hello, Toy

## Controllers

#### `GET /`

* Display `Hello, Toy!` and nothing else.

## Views

Text only.

## Models

None necessary.
