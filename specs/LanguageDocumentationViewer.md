# Toy: {ProgrammingLanguage} Documentation Viewer

## Requirement

The code for the toy must contain or provide self-documentation.

{ProgrammingLanguage} is a placeholder for whatever programming
language the Toy uses, e.g. `Ruby`.

## Controllers

#### `GET /`

* `<title>RFC:Toy {ProgrammingLanguage} Documentation Viewer</title>`.
* Display header `RFC:Toy {ProgrammingLanguage} Documentation Viewer`.
    * Header is a link to `/-`.
* Display subtitle `Dependencies for This Toy`.
* Index list of code that has been loaded, used, required in the toy.
    * List items include package name hyperlinked to view its documentation.

#### `GET /{codePackage}`

* `<title>RFC:Toy {ProgrammingLanguage} Documentation Viewer, {codePackage}</title>`.
* Display header `RFC:Toy {ProgrammingLanguage} Documentation Viewer`.
    * Header is a link to `/`.
* If named {codePackage} exists and has documentation.
    * Display subtitle `{codePackage}`.
    * Display documentation for {codePackage}.
* Else if {codePackage} exists
    * Display message: {codePackage} has no documentation.
* Else
    * Display title `404: Not Found`.
    * Display subtitle `No such package: {codePackage}`.
    * Response status is 404.

#### `GET /-`

* `<title>RFC:Toy {ProgrammingLanguage} Documentation Viewer, Self-Documentation</title>`.
* Display header `RFC:Toy {ProgrammingLanguage} Documentation Viewer`.
    * Header is a link to `/`.
* Display subtitle `Self-Documentation`.
* Display the toy's own documentation.

## Views

HTML only.

All laguage-relevant packages/names MAY be hyperlinked but the
documents from GET / MUST be linked.

All hyperlinks MUST be local.

## Unsettled

Search is out of scope. Maybe a follow-up from this.

## Pitfalls and Hints…

* `;sh rm -rf /` is a possiblity.
* `GET /%3Cscript%3Ealert(%22OHAI%22)%3C/script%3E` is a possiblity.
