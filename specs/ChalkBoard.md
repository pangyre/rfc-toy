# RFC:Toy Chalk Board

There is no error handling or expectations of same in this toy.

## Controllers

#### `GET /*` (All URLs)

* Display the Chalk Board's current message/content.
    * If there has not been a visible message set, display `Welcome to the Chalk Board`.
    * Visible is defined as matching `/\S/`.

#### `POST /*` (All URLs)

* `POST` `enctype` is `text/plain`.
* If an HTML form is inlcuded, the content form parameter name is `content`.
    * Example form shown below.
* `POST` body becomes new Chalk Board message/content if it is visible, `/\S/`.
    * If the post doesn't match `/\S/`, erase / update the Chalk Board content with its default from above.
    * The body is saved as the new literal Chalk Board content, minus possible `content=` key.
* Response is status 303 with `/` as location.

## Views

HTML only. Content MUST be HTML encoded. Content SHOULD be displayed
in a `<pre/>` block or with CSS amounting to similar.

## Models

A persistence layer to save and fetch the content of the Chalk Board.

## Out of Scope Considerations

* Character encoding.
* Content size.

## Discussion

Two ways to solve initial state. Inline || default versus primed
cache/state at start-up.


## Example HTML `<form/>` for Page

```
  <form method="post" action="/" enctype="text/plain">
    <div>
      <textarea name="content"></textarea>
    </div>
  </form>
```
