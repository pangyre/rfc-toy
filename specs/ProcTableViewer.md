# RFC:Toy Process Table Viewer

**Nota bene**: this toy reveals internal/private information about a
server's internals and is therefore a security risk. It MUST not be
coded on any machine/port open to the Internet or the possibility of
malicious users.

## Controllers

All undefined endpoints/conditions are 404.

#### `GET /`

* `<title>RFC:Toy Process Table Viewer</title>`.
* Display header `RFC:Toy Process Table Viewer`.
* Display subtitle `All Readable Processes` *or* `Readable Processes` *or* `{#} Readable Processes`.
* Index list of running processes on the same box as the toy.
    * List items include {pid} and {procName}.
* Each process shows its pid and its name.
    * The name links to its `/{procName}`.
    * The pid links to its `/{procName}/{pid}`.

#### `GET /{procName}`

* `<title>RFC:Toy Process Table Viewer, {procName}</title>`.
* Display header `RFC:Toy Process Table Viewer`.
    * Header is a link to `/`.
* If any processes of the name exist–
    * Display subtitle `{n} Process(es) Named {procName}`.
    * Display a list of the processes with pid and invoked command.
    * Each listing is linked to its `/{procName}/{pid}`.
* Else–
    * 404 response.
    * Display `No processes named {procName} found`.

#### `GET /{procName}/{pid}`

* `<title>RFC:Toy Process Table Viewer, [{pid}] {procName}</title>`.
* Display title `RFC:Toy Process Table Viewer`.
    * Title is a link to `/`.
* If the named process exists **with** the pid–
    * Display subtitle `[{pid}] {procName}`.
        * {procName} is a link to `/{procName}`.
    * Display process's data.
        * MUST include: …
* Else if the named process exists–
    * 404 response.
    * Display `No process named {procName} with pid {pid} found`.
    * {procName} is a link to `/{procName}`.
* Else
    * 404 response.
    * Display `No processes named {procName} found`.

410s only make sense if there is a persistent part… like a loaded page
with the proc that returns 410 when the proc is defunct. So, way out
of scope for this.

## Views

HTML only. No JS or dynamic HTML.

## Unsettled

…

## Examples

    root    205  0.0  0.0  4437208  1196  ??  Ss  Fri06PM  0:00.18 /usr/sbin/someex -flag -arg one

Name is `someex`

We could spec level plain, negotiated content, …? How? What else? Call
it extra credit?


## Extra credit

Probably these should be offloaded… The specs should be extremely
simple. This one, for example, is already bordering on too much and
it's small.

####  `GET /`
* Change subtitle to `{n} Readable Processes`.
    * Locale format…
    * Spell-out…
