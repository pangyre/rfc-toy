# RFC:Toy {ToyName}

## Controllers

All undefined endpoints and conditions are 404.

All undefined methods return 405.

#### `GET /`

* Display…
* Return…
* Instructions, conditions…

#### `GET /{somePath}`

* Display…
* Return…
* Instructions, conditions…

#### `POST /{somePath}`

* Accepts…
* Instructions, conditions…

## Views

## Models

