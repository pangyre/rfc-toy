# RFC:Toy

A list of well defined toy applications.

## Abstract

RFC:Toy affords apples to apples comparisons in code, allowing
different approaches and languages to be coded against narrow,
minimalistic specifications. It is perfect for language and framework
comparison, benchmarking, teaching, learning, test driven development
practice, golfing, design testing, prototyping approaches, assigned
exercises, normalized sample code for frameworks or template engines,
et cetera.

### Nota Bene

Beginner or simplistic code **often** contains security holes. Never
run any toy app on a public server or open it to potentially malicious
users.

### Limitation

Toy specs are *nix centric unless otherwise stated.

This is not RFC-7990 compliant. Not RFC-7841, nor RFC-2026, nor even
RFC-5742. To paraphrase the master: *Don't be all hollering about the
front standards. You'll be lucky to get any back standards. You ain't
gonna get none of it.*

### Call for Help

June 2019 is early in the process of getting this together. It's
unequivocally a good idea; and this kind of document is necessarily
improved with a declarative, authoritative style. That said, the list,
specs, and presentation likely have room for improvement.

Please step up with suggestions or constructive criticism after
reading the notes and getting a handle on the goals.

## Layout of This Repository

* The master list is in this `README.md`.
* Specifications are in `/specs/`.
* Example code for a spec goes in `/ex/{language}/{toyName}`.
* Tests for implimentations go in `/t/{language}/{toyName}`? Maybe.

### Notes…

All minimalist, simplistic, terse as possible. Test and data driven
examples. MUST not attempt to be feature complete for a given
domain at all. Should not all require extra-ASCII handling.

Include test data. As JSON or YAML or both with a translator. Split
tests out into unit and system? Provide Perl mocks such that all tests
can be unit tests? Should the test suite exist in Perl, ready to run
against any implementation? Yes, probably, but not to the exclusion of
testing. Testing should be a co-spec of each. So one could take an
existing solution and write the tests instead.

Adding features not in the plain spec is forbidden. It's no longer a
toy otherwise. If the spec needs adjustment, submit a pull request
with a reasoned case for adding it. “More features is better” is not a
reaonable case for toy apps.

All toys should adhere to best practices *if* it's part of the problem
domain. l10n, for example, would have negotiation and override by user
preference. Two versions? Cookie and path based? No, only path, I think.

## Toys

Recommend/SHOULD a port or a mount point for each so they can all run
on the same box without colliding. Well behaved toys will pass tests
when mounted above root, e.g. `/{toyName}`.

Mention that the URIs are relative to the app…

Arranged in relative order of difficulty and requirement complexity.

* Hello, Toy!
* Chalk Board.
* Process Table Viewer.
* {Language} Documentation Viewer.
* Wiki.
* Link Shortener.
* Link Sharer.
* Web Log Browse and Search.
* Book Search. What text? Bible? Something from Gutenberg? Give 10 options?
* Chat.
* Reviews.
* Shop Journal… Like the guitar stuff you were thinking.
* DICOM Viewer.
* CSV (even Excel?) display.
* Unicode Browser. (Difficulty is Unicode handling + search + browsing by block).
* Git Browser.
* {Simple single-player game}.
* {Simple two-player game}.
* Appointment Scheduler.
* Time Card.
* To Do (with reminders, or they are enhancements).
* Email Client.
* Blog.
* Forum with nested comments.
* Micro CMS; basically blog + forum + issue tracker or something?


#### Things to Exercise

* URI handling.
    * Scheme, host, IP, reverse IP, path parts, escaping, UTF-8.
    * Query parts, reading and constructing.
* Dispatch quirks.
    * Double slashes in paths.
    * Trailing slashes v names; e.g. `/resource` v `/resource/`.
* Reuse. E.g., the link sharer could use the nested comments…?
* REST.
    * PUT, DELETE, and X-Method.
* Content Negotiation.
* Data formats.
    * JSON
    * YAML
    * Delimited
    * CSV
    * XML
* Deployment concerns.
    * Including running 3 previous apps in one server.
* Locale/lang preference.
* DB designs.
* {i18n/l10n} negotiated + session.
* {Something that must deliver/parse HUGE files}? Log browser above?
* {Something that must use perfectly secure cookies}?
* {Minimal pen test proof something…}? OWASP top list? XSRF? XSS? HTTP Strict? Cookies?
* Logging.
* OpenID 1/2?
* OAuth
* LDAP/AD?
* SSL/TLS
* Job queue
* Websocket
    * Echo server?
* Terminal emulation

## Rough Specification Format

* Abstract.
* URL map.
    * ACTION /uri
    * Description with args and response.
* Tests.


## Specifications

…each is getting its own file now.

* Plain spec.
* Undefined behavior is defined. POST, OPTIONS, et cetera.

How to handle other methods? 406? 307/GET? Undefined? Just don't test?

This suggests a higher level of the Toy's tests… defined behavior that
isn't part of the basic implementation spec. This is maybe where best
practices could come into play.

…

It is not remotely required but please include a link-back or textual
message as appropriate to this code repository in any Toy you code.

LEVELS:

Raw code, no libraries.
Standard distribution libraries.
Third party libraries.
Frameworks.

## Grading, Judging

Depends on goals. Teaching and golfing are almost diametrically
opposed, for example.

Style (code, header levels, semantic names, etc), speed, readability,
conciseness, scalability…? SLOC.


### Extra Credit

Understanding deployment issues for applications like `GET /` strongly
implying anchors of `./` instead of `/`.

## Testing

Basic level… ? No undefined behavior in the spec can be tested.

Next level… ? 405 methods, 404 endpoints, et cetera…

## Running Your Toy

Notes for each language… links at least?
