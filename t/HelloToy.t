#!/usr/bin/env perl
use utf8;
use strictures;
use open ":std", ":encoding(utf8)";
use Test::More;
use Test::WWW::Mechanize;

plan skip_all => "Set HELLO_TOY_URI to run tests"
    unless my $app_uri = $ENV{HELLO_TOY_URI};

my $mech = Test::WWW::Mechanize->new( agent => "RFCtoyTester",
                                      timeout => 5 );
# Probably should make a class for this.

$mech->get_ok($app_uri);

# Check content-type

$mech->text_contains("Hello, Toy!");

done_testing(2);

__DATA__

=pod

=encoding utf8

=head1 Name

HelloToy.t - Test for <https://bitbucket.org/pangyre/rfc-toy/src/master/specs/HelloToy.md>.

=head1 Synopsis

 env HELLO_TOY_URI=http://localhost:5000 prove t/HelloToy.t -v

=cut
