#!/usr/bin/env perl
use utf8;
use strictures;
use open ":std", ":encoding(utf8)";
use Test::More;
use Test::WWW::Mechanize;
use HTTP::Request::Common "POST";
use Data::UUID;
use HTML::Entities;

plan skip_all => "Set CHALKBOARD_URI to run tests"
    unless my $app_uri = $ENV{CHALKBOARD_URI};

$app_uri =~ s{(?<!/)\z}{/}g; # Should end in /

note "\nUsing $app_uri as home for RFC:Toy Chalk Board test";

my $mech = Test::WWW::Mechanize->new( agent => "RFCtoyTester",
                                      timeout => 5 );
# Probably should make a class for this.

my $Default = "Welcome to the Chalk Board";

subtest "GET / initial state" => sub {
    $mech->get_ok($app_uri);
    if ( $mech->content =~ /\b \Q$Default\E \b/x )
    {
        pass qq{Page contains "$Default"};
    }
    else
    {
      TODO: {
          local $TODO = "The initial state of the page should contain the text: $Default";
          fail qq{Page contains "$Default"};
        };
    }
    done_testing(2);
};

subtest "Excercise POST/GET" => sub {
    no warnings "uninitialized";

    my $body = "OHAUUID: " . Data::UUID->new->create_str;
    my $post = POST $app_uri,
        "Content-Type" => "text/plain",
        Content => $body;

    $mech->max_redirect(0);
    my $res = $mech->request($post);
    ok $res->is_redirect, "POST / redirects"
        or note $res->as_string;
    ok $res->code == 303, "Redirect is flavor 303"
        or note $res->as_string;
    ok my $location = $res->header("Location"), "Response has a Location";
    is $location, $app_uri, "Location is same as app URI";
    $mech->get_ok( $location );
    $mech->text_contains( $body );
    $mech->max_redirect(1);

    {
        my @error;
        for ( 1 .. 5 )
        {
            $mech->get($mech->uri);
            $mech->content =~ /\Q$body\E/
                or push @error, "Wrong content...";
        }
        ok ! @error, "New content is persistent/consistent over multiple requests";
    }

    subtest "XSS escaping" => sub {
        my $script = "<script>alert('OHAI HAX0RZ!')</script>";
        my $post = POST $app_uri,
            "Content-Type" => "text/plain",
            Content => $script;

        ok $mech->request($post)->is_success,
            "<script/> POST is successful";

        $mech->content_lacks($script, "<script/> is not in content");
        my $decoded = decode_entities( $mech->content );
        like $decoded, qr/\Q$script\E/, "$script is in the page post-decoding";
        done_testing(3);
    };

    {
        my $post = POST $app_uri,
            "Content-Type" => "text/plain",
            Content => "\t";
        ok $mech->request($post)->is_success,
            "Whitespace POST is successful"; # We turned redirects back on.
        $mech->text_contains( $Default );
    }

    done_testing(10);
};

done_testing(2);

__END__

=pod

=encoding utf8

=head1 Name

ChalkBoard.t - test implementations of L<https://bitbucket.org/pangyre/rfc-toy/src/master/specs/ChalkBoard.md>.

=head1 Synopsis

 env CHALKBOARD_URI=http://localhost:1100 prove -v t/ChalkBoard.t

=cut

