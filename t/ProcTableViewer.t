#!/usr/bin/env perl
use utf8;
use strictures;
use open ":std", ":encoding(utf8)";
use Test::More;
use Test::WWW::Mechanize;
use List::Util "shuffle", "uniq";

plan skip_all => "Set PROC_TABLE_VIEWER_URI to run tests"
    unless my $app_uri = $ENV{PROC_TABLE_VIEWER_URI};

$app_uri =~ s{(?<!/)\z}{/}g; # Should end in /

my $mech = Test::WWW::Mechanize->new( agent => "RFCtoyTester",
                                      timeout => 5 );
# Probably should make a class for this.

subtest "GET /" => sub {
    $mech->get_ok($app_uri);
    $mech->title_is("RFC:Toy Process Table Viewer", "Title: RFC:Toy Process Table Viewer");
    $mech->text_contains("RFC:Toy Process Table Viewer", "Page contains: RFC:Toy Process Table Viewer");
    $mech->text_like(qr/(All |\d+ )?Readable Processes/, "Page contains: … Readable Processes");

    # $mech->page_links_ok("All links on page resolve to 200"); ^^^
    # Barfs on kernel 0 … takes forever… Highly unlikely to work
    # reliably given race conditions + underlying server code possibly
    # spawning and reaping kids.

    # Test hueristic, every /{procName}/{pid} has a corresponding
    # /{procName} so they should be in balance/countable.
    my %name_proc;
    subtest "Pid links look right" => sub {
        my @error;
        my @pid_links = $mech->find_all_links( url_regex => qr{(?:\A|/)[\w.]+/\d+\z} );
        ok @pid_links > 5, "There are at least a few pid links: " . scalar @pid_links;
        # ^^^ Even on a bare docker instance or something we really
        # expect to see quite a few processes, pick the lowest
        # seemingly possible number.

        for ( @pid_links )
        {
            my ( $pid ) = $_->url =~ m{/(\d+)\z};
            push @error, $_->url . " doesn't have its pid ($pid) in its text"
                unless $_->text =~ /\b$pid\b/;
            my ( $name ) = $_->url =~ m{([^/]+)/\d+\z};
            push @{$name_proc{$name}}, $pid;
        }

        ok ! @error, "All {pid} links look good"
            or note join $/, @error;

        done_testing(2);
    };

    subtest "Process name links look right" => sub {
        my @error;
        for my $procName ( keys %name_proc )
        {
            my @name_links = $mech->find_all_links( url_regex => qr{(?:\A|/)\Q$procName\E\z} );

            push @error, sprintf("%s link count (%d) doesn't match its pid links (%d)",
                                 $procName, scalar(@name_links), scalar(@{$name_proc{$procName}}) )
                unless @name_links == @{$name_proc{$procName}};

            for ( @name_links )
            {
                push @error, $_->url . " doesn't have its name, $procName, in its text: " . $_->text
                    unless $_->text =~ /(\A|\b)\Q$procName\E(\b|\z)/;
                #push @error, "perl -E 'say q{OK} if " . '"' . $_->text . "\" =~ /\\b\\Q$procName\\E\\b/'"
                #    unless $_->text =~ /\b\Q$procName\E\b/;
            }
        }
        ok ! @error, "All {procName} links look good and match {pid} counts"
            or note join $/, @error;

        done_testing(1);
    };

    done_testing(6);
};

subtest "GET /{procName}" => sub {
    $mech->get_ok($app_uri);
    my @proc_names = shuffle uniq
        map { $_->url =~ m{([^/]+)/\d+\z}; $1 }
        $mech->find_all_links( url_regex => qr{(?:\A|/)[\w.]+/\d+\z} );

    my @error;
    my $check = @proc_names > 15 ? 15 : scalar @proc_names;
    for my $name ( @proc_names[0..$check] )
    {
        my $uri = $app_uri . $name;
        my $res = $mech->get($uri);
        push @error, sprintf("Couldn't get %s (%d)",
                             $uri, $res->code)
            unless $res->is_success;
    }
    ok ! @error, "$check randomly ordered /{procName} URIs were OK"
        or note join $/, @error;

    # Low confidence just checking one…
    subtest "Drill down with last request " . $mech->uri->path => sub {
        my ( $name ) = $mech->uri->path =~ m{([^/]+)};

        $mech->title_is("RFC:Toy Process Table Viewer, $name", "Title: RFC:Toy Process Table Viewer, $name");
        my $header = $mech->find_link( text => "RFC:Toy Process Table Viewer" );
        is eval { $header->url_abs }, $app_uri, qq{Found header "RFC:Toy Process Table Viewer" linking to $app_uri};
        $mech->text_like(qr/\d+ Process\(?(?:es)?\)? Named \Q$name\E/);
        my ( $count ) = $mech->text =~ /(\d+) Process\(?(?:es)?\)? Named \Q$name\E/;

        # Display a list of the processes with pid and invoked command.
        # Each listing is linked to its /{procName}/{pid}.
        note qr{(?:\A|/)\Q$name\E/\d+\z} ;
        my @pid_links = $mech->find_all_links( url_regex => qr{(?:\A|/)\Q$name\E/\d+\z} );
        no warnings "uninitialized";
        is $count, scalar @pid_links,
            sprintf "There is/are %s matching /{procName}/{pid} link(s)", $count || "[undef]";
        done_testing();
    };
    
    my $response = $mech->get("/ThisIsAProcessNameYouWillProbablyNeverSee");
    is $response->code, 404, "GET Non-existent {procName} is 404";

    done_testing(4);
};

subtest "GET /{procName}/{pid}" => sub {
    $mech->get_ok($app_uri);
    my @links = map { $_->url =~ m{([^/]+)/(\d+)\z}; { pid => $2, procName => $1, link => $_ } }
        $mech->find_all_links( url_regex => qr{(?:\A|/)[\w.]+/\d+\z} );
    # note explain \%pid_info;
    my @error;
    my $check = @links > 15 ? 15 : scalar @links;
    for my $link ( @links[0..$check] )
    {
        my $res = $mech->get($link->{link});
        push @error, sprintf("Couldn't get %s (%d)",
                             $link->{link}, $res->code)
            unless $res->is_success;
    }
    ok ! @error, "$check randomly ordered /{procName}/{pid} URIs were OK"
        or note join $/, @error;

    subtest "Drill down with single request " . $mech->uri->path => sub {
        my $link = @links[rand @links];
        $mech->get_ok($link->{link});
        my $name = $link->{procName};
        my $pid = $link->{pid};
        $mech->title_is("RFC:Toy Process Table Viewer, [$pid] $name",
                        "RFC:Toy Process Table Viewer, [$pid] $name");
        my $header = $mech->find_link( text => "RFC:Toy Process Table Viewer" );
        is eval { $header->url_abs }, $app_uri, qq{Found header "RFC:Toy Process Table Viewer" linking to $app_uri};
        $mech->text_contains("[$pid] $name");
        # Display subtitle [{pid}] {procName}.
        # FOUND A BUG
        #     searched: "RFC:Toy Process Table Viewer, [674] MTLCompilerSer"...
        #   can't find: "[674] MTLCompilerService"
        #         LCSS: "[674] MTLCompilerServi"

        # {procName} is a link to /{procName}.
        ok $mech->find_link( text => $name ), "There is a link named {procName} ($name)";
        # Display process's data.
        # MUST include: … *nix v WIN sets?
        done_testing(5);
    };
    
    subtest "/{wrongName}/{realPid}" => sub {
        my $link = @links[rand @links];
        my $name = "n0pe";
        my $uri = URI->new_abs( "/$name/$link->{pid}", $mech->uri );
        my $res = $mech->get($uri);
        is $res->code, 404, "GET " . $uri->path . " -> 404";
        $mech->text_contains("No processes named $name found");
        done_testing();
    };

    subtest "/{rightName}/{wrongPid}" => sub {
        my $link = @links[rand @links];
        my $link2 = @links[rand @links];
        $link2 = @links[rand @links] until $link->{procName} ne $link2->{procName}; # Dangerous, que no?

        my $name = $link->{procName};
        my $pid = $link2->{pid};
        my $uri = URI->new_abs( "/$name/$pid", $mech->uri );
        my $res = $mech->get($uri);
        is $res->code, 404, "GET " . $uri->path . " -> 404";
        $mech->text_contains("No process named $name with pid $pid found");

        done_testing(2);
    };

    subtest "/{rightName}/{garbagePid}" => sub {
        my $link = @links[rand @links];
        my $name = $link->{procName};
        my $pid = $link->{pid} . "xyz";
        my $uri = URI->new_abs( "/$name/$pid", $mech->uri );
        my $res = $mech->get($uri);
        is $res->code, 404, "GET " . $uri->path . " -> 404";
        $mech->text_contains("No process named $name with pid $pid found");

        done_testing(2);
    };

    done_testing(6);
};

subtest "Generic 404 handling" => sub {
    my @error;
    my @paths = ( "/nothingburger", "/{procName}?foo=bar", "/.", "/-", " ", "/ /", qx{pwd} );
    for my $path ( @paths )
    {
        my $uri = URI->new_abs( $path, $mech->uri );
        my $res = $mech->get( $uri );
        push @error, "Found «$path» when it should 404"
            if $res->code != 404;
    }
    ok ! @error, "Default handler seems to catch 404s"
        or note join $/, @error;

    done_testing(1);
};

done_testing(4);

__DATA__

=pod

=encoding utf8

=head1 Name


=head1 Synopsis

First, ensure your toy is running on some sort of web or application
server. If you can't reach it in a browser, the test won't be able to
either.

 export PROC_TABLE_VIEWER_URI=…
 # or
 setenv PROC_TABLE_VIEWER_URI …
 # then
 prove …

 # OR

 env PROC_TABLE_VIEWER_URI=http://localhost:1101 prove t/ProcTableViewer.t -v

=head1 Spec for Toy to Test

L<https://bitbucket.org/pangyre/rfc-toy/src/master/specs/ProcTableViewer.md>

L<Test::WWW::Mechanize>

=head1 Copyright

=cut
