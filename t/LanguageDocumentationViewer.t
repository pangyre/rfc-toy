#!/usr/bin/env perl
use utf8;
use strictures;
no warnings "uninitialized";
use open ":std", ":encoding(utf8)";
use Test::More;
use Test::WWW::Mechanize;
use List::Util "shuffle", "uniq";
use URI::Escape;
use URI;

plan skip_all => "Set LANGUAGE_DOCUMENTATION_VIEWER_URI to run tests"
    unless my $app_uri = $ENV{LANGUAGE_DOCUMENTATION_VIEWER_URI};

$app_uri =~ s{(?<!/)\z}{/}g; # Should end in /

my $mech = Test::WWW::Mechanize->new( agent => "RFCtoyTester",
                                      timeout => 5 );
# Probably should make a class for this.

subtest "GET /" => sub {
    $mech->get_ok($app_uri);
    $mech->title_like(qr/RFC:Toy \S+ Documentation Viewer/);
    $mech->text_contains("Dependencies for This Toy");
    $mech->text_like(qr/\S+ Documentation Viewer/);
    ok my $header = $mech->find_link( text_regex => qr/RFC:Toy \S+ Documentation Viewer/,
                                      url_regex => qr{\A\.?/?-\z} ),
        "Header links to /-";

    $mech->get_ok($header);

    {
        ok my $header = $mech->find_link( text_regex => qr/RFC:Toy \S+ Documentation Viewer/,
                                          url_regex => qr{\A\.?/?\z} ),
            "Header links to /";

        $mech->title_like(qr/RFC:Toy \S+ Documentation Viewer, Self-Documentation/);

        $mech->text_contains("Self-Documentation");
        $mech->get_ok($header); # Go back to /
    }

    my @packages = shuffle $mech->find_all_links( url_regex => qr{\A\.?/?(?!-)[^/]+\z} );
    ok @packages, "Found " . scalar(@packages) . " links to view packages";

    subtest "GET /{codePackage}" => sub {
        my $max = @packages > 15 ? 15 : scalar(@packages); # Test a batch at random.
        my @error;

        for my $package ( @packages[0..$max] )
        {
            my $res = $mech->get($package);
            push @error, sprintf("Couldn't get %s (%d)",
                                 $package->url, $res->code)
                unless $res->is_success;
        }
        ok ! @error, "$max randomly ordered /{codePackage} URIs were OK"
            or note join $/, @error;

        done_testing(1);
    };

    my ( $name ) = uri_unescape($mech->uri->path) =~ m{([^/]+)}; # Might need a decode pass too…
    subtest "Drill down with last request $name" => sub {
        $mech->title_like(qr/\ARFC:Toy \S+ Documentation Viewer, \Q$name\E/);
        # my $header = $mech->find_link( text => "RFC:Toy Process Table Viewer" );
        # is eval { $header->url_abs }, $app_uri, qq{Found header "RFC:Toy Process Table Viewer" linking to $app_uri};
        done_testing(1);
    };

    {
        my $uri = URI->new_abs("/ThisIsAPackageNameYouWillProbablyNeverSee", $mech->uri);
        my $response = $mech->get( $uri );
        is $response->code, 404, "GET Non-existent /{codePackage} is 404"
            or note $uri;
        $mech->text_contains("No such package: ThisIsAPackageNameYouWillProbablyNeverSee");
    }

    done_testing(15);
};

done_testing(1);

__END__

    


subtest "GET /{procName}" => sub {
    $mech->get_ok($app_uri);
    my @proc_names = shuffle uniq
        map { $_->url =~ m{([^/]+)/\d+\z}; $1 }
        $mech->find_all_links( url_regex => qr{(?:\A|/)[\w.]+/\d+\z} );

    my @error;
    my $check = @proc_names > 15 ? 15 : scalar @proc_names;
    for my $name ( @proc_names[0..$check] )
    {
        my $uri = $app_uri . $name;
        my $res = $mech->get($uri);
        push @error, sprintf("Couldn't get %s (%d)",
                             $uri, $res->code)
            unless $res->is_success;
    }
    ok ! @error, "$check randomly ordered /{procName} URIs were OK"
        or note join $/, @error;

    # Low confidence just checking one…
    subtest "Drill down with last request " . $mech->uri->path => sub {
        my ( $name ) = $mech->uri->path =~ m{([^/]+)};

        $mech->title_is("RFC:Toy Process Table Viewer, $name", "Title: RFC:Toy Process Table Viewer, $name");
        my $header = $mech->find_link( text => "RFC:Toy Process Table Viewer" );
        is eval { $header->url_abs }, $app_uri, qq{Found header "RFC:Toy Process Table Viewer" linking to $app_uri};
        $mech->text_like(qr/\d+ Process\(?(?:es)?\)? Named \Q$name\E/);
        my ( $count ) = $mech->text =~ /(\d+) Process\(?(?:es)?\)? Named \Q$name\E/;

        # Display a list of the processes with pid and invoked command.
        # Each listing is linked to its /{procName}/{pid}.
        note qr{(?:\A|/)\Q$name\E/\d+\z} ;
        my @pid_links = $mech->find_all_links( url_regex => qr{(?:\A|/)\Q$name\E/\d+\z} );
        no warnings "uninitialized";
        is $count, scalar @pid_links,
            sprintf "There is/are %s matching /{procName}/{pid} link(s)", $count || "[undef]";
        done_testing(11);
    };
    
    my $response = $mech->get("/ThisIsAProcessNameYouWillProbablyNeverSee");

    is $response->code, 404, "GET Non-existent {procName} is 404";

    done_testing(4);
};

subtest "GET /{procName}/{pid}" => sub {
    $mech->get_ok($app_uri);
    my @links = map { $_->url =~ m{([^/]+)/(\d+)\z}; { pid => $2, procName => $1, link => $_ } }
        $mech->find_all_links( url_regex => qr{(?:\A|/)[\w.]+/\d+\z} );
    # note explain \%pid_info;
    my @error;
    my $check = @links > 15 ? 15 : scalar @links;
    for my $link ( @links[0..$check] )
    {
        my $res = $mech->get($link->{link});
        push @error, sprintf("Couldn't get %s (%d)",
                             $link->{link}, $res->code)
            unless $res->is_success;
    }
    ok ! @error, "$check randomly ordered /{procName}/{pid} URIs were OK"
        or note join $/, @error;

    subtest "Drill down with single request " . $mech->uri->path => sub {
        my $link = @links[rand @links];
        $mech->get_ok($link->{link});
        my $name = $link->{procName};
        my $pid = $link->{pid};
        $mech->title_is("RFC:Toy Process Table Viewer, [$pid] $name",
                        "RFC:Toy Process Table Viewer, [$pid] $name");
        my $header = $mech->find_link( text => "RFC:Toy Process Table Viewer" );
        is eval { $header->url_abs }, $app_uri, qq{Found header "RFC:Toy Process Table Viewer" linking to $app_uri};
        $mech->text_contains("[$pid] $name");
        # Display subtitle [{pid}] {procName}.
        # FOUND A BUG
        #     searched: "RFC:Toy Process Table Viewer, [674] MTLCompilerSer"...
        #   can't find: "[674] MTLCompilerService"
        #         LCSS: "[674] MTLCompilerServi"

        # {procName} is a link to /{procName}.
        ok $mech->find_link( text => $name ), "There is a link named {procName} ($name)";
        # Display process's data.
        # MUST include: … *nix v WIN sets?
        done_testing(5);
    };
    
    subtest "/{wrongName}/{realPid}" => sub {
        my $link = @links[rand @links];
        my $name = "n0pe";
        my $uri = URI->new_abs( "/$name/$link->{pid}", $mech->uri );
        my $res = $mech->get($uri);
        is $res->code, 404, "GET " . $uri->path . " -> 404";
        $mech->text_contains("No processes named $name found");
        done_testing();
    };

    subtest "/{rightName}/{wrongPid}" => sub {
        my $link = @links[rand @links];
        my $link2 = @links[rand @links];
        $link2 = @links[rand @links] until $link->{procName} ne $link2->{procName}; # Dangerous, que no?

        my $name = $link->{procName};
        my $pid = $link2->{pid};
        my $uri = URI->new_abs( "/$name/$pid", $mech->uri );
        my $res = $mech->get($uri);
        is $res->code, 404, "GET " . $uri->path . " -> 404";
        $mech->text_contains("No process named $name with pid $pid found");

        done_testing(2);
    };

    subtest "/{rightName}/{garbagePid}" => sub {
        my $link = @links[rand @links];
        my $name = $link->{procName};
        my $pid = $link->{pid} . "xyz";
        my $uri = URI->new_abs( "/$name/$pid", $mech->uri );
        my $res = $mech->get($uri);
        is $res->code, 404, "GET " . $uri->path . " -> 404";
        $mech->text_contains("No process named $name with pid $pid found");

        done_testing(2);
    };

    done_testing(6);
};

subtest "Generic 404 handling" => sub {
    my @error;
    my @paths = ( "/nothingburger", "/{procName}?foo=bar", "/.", "/-", " ", "/ /", qx{pwd} );
    for my $path ( @paths )
    {
        my $uri = URI->new_abs( $path, $mech->uri );
        my $res = $mech->get( $uri );
        push @error, "Found «$path» when it should 404"
            if $res->code != 404;
    }
    ok ! @error, "Default handler seems to catch 404s"
        or note join $/, @error;

    done_testing(1);
};

done_testing(4);

__DATA__

=pod

=encoding utf8

=head1 Name

LanguageDocumentationViewer.t - …

=head1 Synopsis

First, ensure your toy is running on some sort of web or application
server. If you can't reach it in a browser, the test won't be able to
either.

 env LANGUAGE_DOCUMENTATION_VIEWER_URI=http://localhost:1103 \
     prove t/LanguageDocumentationViewer.t -v

=head1 Spec for Toy to Test

L<https://bitbucket.org/pangyre/rfc-toy/src/master/specs/LanguageDocumentationViewer.md>

L<Test::WWW::Mechanize>…

=head1 Copyright

=cut
