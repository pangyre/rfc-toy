#!/usr/bin/env perl
use utf8;
use strict;
use 5.14.0;
use Plack::Request;
use HTML::Entities;
use URI;

my $Name = "RFC:Toy Chalk Board";

my $Default = "Welcome to the Chalk Board";

package MyDB v0.0.1 {
    use File::Spec;
    use Path::Tiny;

    sub new {
        db()->touch;
        bless \my $var, __PACKAGE__;
    }

    sub db { path( File::Spec->tmpdir, "ChalkBoard.data" ) }

    sub content {
        my $self = shift;
        if ( my $content = shift )
        {
            $self->db->spew_utf8($content);
            $$self = $content;
        }
        $$self ||= $self->db->slurp_utf8 || $Default;
    }
};

sub _template {
    my %arg = @_;
    $_ = encode_entities($_, '><&') for values %arg;
    <<"_HTML_";
<!DOCTYPE html>
<html>
  <head>
    <title>RFC:Toy Chalk Board</title>
    <style>
      * { font-family:roboto, "helvetica neue", sans-serif; font-weight:300 }
      html { margin:4% auto; max-width:60em; }
      pre,tt,code { font-family:monaco, monospace; white-space:pre-line }
      a { text-decoration:none }
      a:hover { text-decoration:underline }
      textarea { width:80%; height:6em }
    </style>
  </head>
  <body>
    <h1>
      RFC:Toy Chalk Board
    </h1>
    <pre>$arg{content}</pre>
    <form method="post" action="/" enctype="text/plain">
      <div>
        <textarea name="content"></textarea>
      </div>
      <div>
        <input type="submit" value="update">
      </div>
    </form>
  </body>
</html>
_HTML_
}

my $DB = MyDB->new;
my %Content_Type = ( "Content-Type" => "text/html" );

sub {
    my $req = Plack::Request->new(shift);

    if ( $req->method eq "GET" )
    {
        [ 200, [ %Content_Type ], [ _template( content => $DB->content ) ] ];
    }
    elsif ( $req->method eq "POST" )
    {
        ( my $content = $req->content ) =~ s/\Acontent=//;
        $DB->content( $content =~ /\S/ ? $content : $Default );
        [ 303, [ Location => URI->new_abs( "/", $req->uri ) ], [] ];
    }
    else
    {
        [ 405, [ %Content_Type ],
          [ "405: Request Method ", $req->method, " Not Allowed" ] ];
    }

};

__END__

=pod

=encoding utf8

=head1 Name

ChalkBoard.plack.psgi - implementation of L<RFC:Toy
Chalk Board|https://bitbucket.org/pangyre/rfc-toy/> using low level L<PSGI>.

=head1 Synopsis

 git clone git@bitbucket.org:pangyre/rfc-toy.git
 plackup rfc-toy/ex/perl/ChalkBoard.plack.psgi -r

=head1 Requirements

Minimum Perl version is B<5.14.2> to support C<package {}> semantics.

Required modules: L<Plack::Request>, L<HTML::Entities>, and
L<Path::Tiny>.

=head1 Notes

…

=head1 Code Repository

This toy: L<https://bitbucket.org/pangyre/rfc-toy/src/master/ex/perl/ChalkBoard.plack.psgi>.

Its specification: L<https://bitbucket.org/pangyre/rfc-toy/src/master/specs/ChalkBoard.md>.

=head1 Author and Copyright

©2019 Ashley Pond V.

=head1 License and Disclaimer of Warranty

You may redistribute and modify this package under the same terms as Perl itself.

This software is licensed free of charge. This software has no
warranty, protections, or guarantees whatsoever.

=cut
