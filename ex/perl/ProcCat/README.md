# How This Was Done

(Move this to ProcCat.pod?)

```
git clone git@bitbucket.org:pangyre/rfc-toy.git
cd rfc-toy/ex/perl

catalyst.pl ProcCat

# …lots of output as template for Catalyst application named ProcCat
# is created. A lot of the defaults are overly opinionated and
# non-standard in the Perlsphere.

# Take out all potentially confusing boilerplate we won't use–
rm -rf ProcCat/script
rm -rf ProcCat/t
rm -rf ProcCat/root/
rm -rf ProcCat/root/static
rm -rf ProcCat/README
rm -rf ProcCat/Changes 
```

Start this document–

```
emacs ex/perl/ProcCat/README.md
```

In my view Config::General was a very wrong turn for a Catalyst
default config file so I revert to YAML. The file will need a tweak to
work right.

```
mv ProcCat/proccat.conf ProcCat/proccat.yml
```

We're left with this, including this README.md–

```
find ex/perl/ProcCat
ex/perl/ProcCat
ex/perl/ProcCat/proccat.yml
ex/perl/ProcCat/proccat.psgi
ex/perl/ProcCat/README.md
ex/perl/ProcCat/root
ex/perl/ProcCat/root/favicon.ico
ex/perl/ProcCat/lib
ex/perl/ProcCat/lib/ProcCat.pm
ex/perl/ProcCat/lib/ProcCat
ex/perl/ProcCat/lib/ProcCat/Controller
ex/perl/ProcCat/lib/ProcCat/Controller/Root.pm
ex/perl/ProcCat/lib/ProcCat/Model
ex/perl/ProcCat/lib/ProcCat/View
```

## MVC Concerns

A process viewer is a great toy match for the MVC pattern. The Model
is the process reader. The Viewer displays human formatted information
from the Model. The Control decides which information to get from the
which Model and which View should handle it.

This is a toy so we will have only one of each, MVC.

### Controller

We already have a root controller: `lib/ProcCat/Controller/Root.pm`.
That's all we need. The
[specification](https://bitbucket.org/pangyre/rfc-toy/src/master/specs/ProcTableViewer.md)
tells us there are just defined three endpoints. In this case, they
happen to line up, though imperfectly if we were optimizing, with a
powerful Catalyst feature, chained controllers, and we'll use it.

### View

We are only going to offer HTML. We create `lib/ProcCat/View/HTML.pm`.
There are helper scripts for this; well, we deleted them but they were
there. In my view it's better to read the docs and do it manually
instead. Helpers are too magical for beginners and tend toward
unecessary or badly optimized for experts.

The View will be L<Template::Toolkit> based because it's the most
popular/standard Perl template engine. There are plenty of other
options.


### Model

A model is just a source of information; data in a low level, plain
format. The Model will be based on the Perl module Proc::ProcessTable.
Many Catalyst models use RDBM underneath and the community standard,
favored harness for that is DBIx::Class. It's unecessary and way out
of scope for this particular toy; only mentioned for contrast and
context.


### App Module

We are doing basically nothing here. …Note about config hierarchy and
test config. …and how we could have easily forgone the config loader
to put the same info in ProcCat.


```
created "ProcCat"
created "ProcCat/script"
created "ProcCat/lib"
created "ProcCat/root"
created "ProcCat/root/static"
created "ProcCat/root/static/images"
created "ProcCat/t"
created "ProcCat/lib/ProcCat"
created "ProcCat/lib/ProcCat/Model"
created "ProcCat/lib/ProcCat/View"
created "ProcCat/lib/ProcCat/Controller"
created "ProcCat/proccat.conf"
created "ProcCat/proccat.psgi"
created "ProcCat/lib/ProcCat.pm"
created "ProcCat/lib/ProcCat/Controller/Root.pm"
created "ProcCat/README"
created "ProcCat/Changes"
created "ProcCat/t/01app.t"
created "ProcCat/t/02pod.t"
created "ProcCat/t/03podcoverage.t"
created "ProcCat/root/static/images/catalyst_logo.png"
created "ProcCat/root/static/images/btn_120x50_built.png"
created "ProcCat/root/static/images/btn_120x50_built_shadow.png"
created "ProcCat/root/static/images/btn_120x50_powered.png"
created "ProcCat/root/static/images/btn_120x50_powered_shadow.png"
created "ProcCat/root/static/images/btn_88x31_built.png"
created "ProcCat/root/static/images/btn_88x31_built_shadow.png"
created "ProcCat/root/static/images/btn_88x31_powered.png"
created "ProcCat/root/static/images/btn_88x31_powered_shadow.png"
created "ProcCat/root/favicon.ico"
created "ProcCat/Makefile.PL"
created "ProcCat/script/proccat_cgi.pl"
created "ProcCat/script/proccat_fastcgi.pl"
created "ProcCat/script/proccat_server.pl"
created "ProcCat/script/proccat_test.pl"
created "ProcCat/script/proccat_create.pl"
Change to application directory and Run "perl Makefile.PL" to make
sure your install is complete
```
