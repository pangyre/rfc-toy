package ProcCat;
use Moose;
use namespace::autoclean;
use Catalyst::Runtime 5.80;

# Don"t use -Debug in production.
use Catalyst "ConfigLoader";

extends "Catalyst";

our $VERSION = "0.01";

__PACKAGE__->config( name => "ProcCat" );

__PACKAGE__->setup();

1;

__END__

=pod

=encoding utf8

=head1 Name

ProcCat - implementation of L<RFC:Toy Process Table
Viewer|https://bitbucket.org/pangyre/rfc-toy/> using L<Catalyst>.

=head1 Synopsis

 git clone git@bitbucket.org:pangyre/rfc-toy.git
 plackup -I ex/perl/ProcCat/lib/ ex/perl/ProcCat/proccat.psgi -r

=head1 Requirements

Minimum Perl version is B<…>.

Required modules: L<Catalyst>, L<Proc::ProcessTable>, L<Template>, and
the I<many> dependencies they have.

=head1 Notes

…

=head1 Code Repository

This toy: L<https://bitbucket.org/pangyre/rfc-toy/src/master/ex/perl/ProcCat>.

Its specification: L<https://bitbucket.org/pangyre/rfc-toy/src/master/specs/ProcTableViewer.md>.

=head1 Author and Copyright

©2019 Ashley Pond V.

=head1 License and Disclaimer of Warranty

You may redistribute and modify this package under the same terms as Perl itself.

This software is licensed free of charge. This software has no
warranty, protections, or guarantees whatsoever.

=cut
