package ProcCat::Model::ProcTable;
use strict;
use warnings;
use parent "Catalyst::Model";
use Proc::ProcessTable;
use Path::Tiny;

use Moo;
# Clearer? Lifespan?
has procTable =>
    is => "lazy",
    ;

sub _build_procTable { "Proc::ProcessTable"->new }

sub processes {
    my $self = shift;
    $self->procTable->table;
}

sub fields { "realfname", grep $_ ne "cmndline", +shift->procTable->fields }

sub processes_by_name {
    my $self = shift;
    my $name = shift || return;
    return unless my @p = grep { $name eq $_->realfname } @{ $self->processes };
    \@p;
}

sub process_for_pid {
    my $self = shift;
    my $pid = shift // return;
    [ grep $pid eq $_->pid, @{ $self->processes } ]->[0];
}

sub Proc::ProcessTable::Process::realfname {
    my $proc = shift;
    my $cmd = $proc->cmndline;
    my $name = $cmd =~ m{\A(?!=/)([^/ ]+)} ? $1 : eval {
        path( [ split / /, $cmd ]->[0] )->basename;
    };
    warn $cmd, " -> ", $@ if $@;
    $name and length $name > length $proc->fname ? $name : $proc->fname;
};

1;

__END__
