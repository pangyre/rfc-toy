package ProcCat::Controller::Root;
use Moose;
use namespace::autoclean;
BEGIN { extends "Catalyst::Controller" }

__PACKAGE__->config( namespace => "" );

sub default :Path {
    my ( $self, $c ) = @_;
    $c->stash( template => "default.tt" );
    $c->response->status(404);
}

sub processes :Path :Args(0) {
    my ( $self, $c ) = @_;
    $c->stash( processes => $c->model("ProcTable")->processes );
}

sub base_process :Chained("/") :PathPart("") :CaptureArgs(1) {
    my ( $self, $c, $proc_name ) = @_;
    # $c->stash( proc_name => $proc_name );
    my $processes = $c->model("ProcTable")->processes_by_name( $proc_name );
    unless ( $processes )
    {
        $c->stash( message => "No processes named $proc_name found" );
        $c->detach("default");
    }
    $c->stash( proc_name => $proc_name,
               processes => $processes );
}

sub base_process_with_pid :Chained("base_process") :PathPart("") :CaptureArgs(1) {
    my ( $self, $c, $pid ) = @_;
    my $proc = $c->model("ProcTable")->process_for_pid( $pid );
    unless ( $proc and $proc->realfname eq $c->stash->{proc_name} )
    {
        $c->stash( message => join(" ", "No process named",
                                   sprintf('<a href="%s">%s</a>',
                                           $c->uri_for_action("processes_by_name", [$c->stash->{proc_name}]),
                                           $c->stash->{proc_name}),
                                   "with pid $pid found" ) );
        $c->detach("default");
    }
    $c->stash( pid => $pid,
               process => $proc );
}

sub processes_by_name :Chained("base_process") :PathPart("") :Args(0) {}

sub process_by_name_pid :Chained("base_process_with_pid") :PathPart("") :Args(0) {
    my ( $self, $c ) = @_;
    my $proc = $c->stash->{process};
    $c->stash( title => sprintf("%s, [%d] %s",
                                $c->config->{name},
                                $proc->pid,
                                $proc->realfname) );
}

sub end : ActionClass("RenderView") {}

1;

__END__

=encoding utf-8

=pod

=encoding utf8

=head1 Name

ProcCat::Controller::Root - only Controller for ProcCat.

=head1 Endpoints

=over 4

=item processes

…

=item default

So all undefined endpoints will 404. Why not name the method
C<not_found> you ask? Good question. It would be much more semantic
but I think it's important to understand that all requests to a web
app will 404 unless they are defined. The name C<default> underscores
the correctness of this whitelist-style approach.

=back

=head1 Methods

=over 4

=item end

Attempt to render a view, if needed.

=back

=cut
