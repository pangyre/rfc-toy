use strict;
use warnings;
use ProcCat;

ProcCat->apply_default_middlewares( ProcCat->psgi_app );

__END__

=pod

=encoding utf8

=head1 Name

proccat.psgi - simple psgi wrapper to run L<ProcCat>.

=head1 See Also

L<ProcCat>.

=cut
