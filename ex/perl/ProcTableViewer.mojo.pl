use strict;
use Mojolicious::Lite;
use Proc::ProcessTable;
use Number::Format "format_number"; # Really doesn't belong…? Maybe…?
use Path::Tiny;

helper format_number => sub {
    format_number( $_[1] );
};
helper endpoint => sub { +shift->match->endpoint->name };
helper procs => sub { Proc::ProcessTable->new };
helper procTable => sub { +shift->procs->table };
helper procName => sub {
    my $c = shift;
    my $name = shift;
    grep $_->realfname eq $name, @{ $c->procTable };
};
helper pid => sub {
    my $c = shift;
    my $pid = shift;
    return unless $pid =~ /\A[1-9][0-9]*\z/;
    [ grep $_->pid == $pid, @{ $c->procTable } ]->[0]; # or List::Util::first
};

sub Proc::ProcessTable::Process::realfname {
    my $proc = shift;
    my $cmd = $proc->cmndline;
    my $name = $cmd =~ m{\A(?!=/)([^/ ]+)} ? $1 : eval {
        path( [ split / /, $cmd ]->[0] )->basename;
    };
    warn $cmd, " -> ", $@ if $@;
    length $name > length $proc->fname ? $name : $proc->fname;
};

get "/";

get "/:procName" => [ procName => qr{[^/]+} ] => sub {
    my $c = shift;
    my $procName = $c->stash("procName");
    my @procs = $c->procName( $procName );
    unless ( @procs )
    {
        $c->stash( message => "No processes named $procName found" );
        return $c->reply->not_found;
    }

    $c->stash( processes => [ $c->procName( $c->stash("procName") ) ],
               fields => [ $c->procs->fields ] );
};

get "/:procName/:pid" => [ procName => qr{[^/]+} ] => sub {
    my $c = shift;
    my $procName = $c->stash("procName");
    my @procs = $c->procName( $procName ); # Doubled up, lame…
    unless ( @procs )
    {
        $c->stash( message => "No processes named $procName found" );
        return $c->reply->not_found;
    }

    my $pid = $c->stash("pid");
    my $proc = $c->pid( $c->stash("pid") );
    unless ( $proc and $proc->realfname eq $procName )
    {
        $c->stash( message => "No process named $procName with pid $pid found" );
        return $c->reply->not_found;
    }
    $c->stash( fields => [ $c->procs->fields ],
               proc => $proc );
};

# any "/*" => sub { +shift->res->code(404) } => "not_found";
any "/*" => sub { +shift->reply->not_found };
# ^^^ This is redundant with the template, right?

app->start;

__DATA__

=pod

=encoding utf8

=head1 Name

ProcTableViewer.mojo.pl - implementation of L<RFC:Toy Process Table
Viewer|https://bitbucket.org/pangyre/rfc-toy/> using L<Mojolicious::Lite>.

=head1 Synopsis

 git clone git@bitbucket.org:pangyre/rfc-toy.git
 plackup rfc-toy/ex/perl/ProcTableViewer.mojo.pl -r

=head1 Required Non-Core Perl Libaries

L<Mojolicious::Lite>, L<Proc::ProcessTable>, L<Number::Format>, L<Path::Tiny>.

=head1 Code Repository

L<https://bitbucket.org/pangyre/rfc-toy/src/master/ex/perl/ProcTableViewer.mojo.pl>.

=head1 Author and Copyright

©2019 Ashley Pond V.

=head1 License and Disclaimer of Warranty

You may redistribute and modify this package under the same terms as Perl itself.

This software is licensed free of charge. This software has no
warranty, protections, or guarantees whatsoever.

=cut

@@ _short_proc.html.ep
   <span title="<%= $proc->cmndline %>" class="pid">
         <a href="/<%= $proc->realfname %>/<%= $proc->pid %>"><%= $proc->pid %></a>
   </span>
   <a href="/<%= $proc->realfname %>"><%= $proc->realfname %></a>

@@ _long_proc.html.ep
% my $endpoint = $c->match->endpoint->name;
% for my $proc ( @{$procs} ) {
<div class="process">

% if ( $endpoint eq "procNamepid" ) {
  <h3><%= $proc->cmndline %></h3>
  <div style="columns:4">
  % for my $field ( grep $_ !~ /\b(cmndline|pid)\b/, sort { lc $a cmp lc $b } @{$fields} ) {
    <div style="white-space:nowrap">
      <span style="min-width:8em; opacity:0.5; text-align:right; display:inline-block"><%= $field %></span>
      <span><%= $proc->$field %></span>
    </div>
  % }
  </div>
% } else {
  <h4><a href="<%= $c->url_for("procNamepid", procName => $proc->fname, pid => $proc->pid) %>"><%= "[" . $proc->pid . "] " unless $endpoint eq "procNamepid" %>
      <%= $proc->cmndline %></a></h4>
% }
</div>
% }

@@ .html.ep
% layout "wrapper", title => "RFC:Toy Process Table Viewer";
<h2><%= $c->format_number( scalar @{ $c->procTable } ) %> Readable Processes</h2>
<div style="columns:2">
% for my $proc ( sort { lc $a->fname cmp lc $b->fname } @{ $c->procTable } ) {
  <div style="white-space:nowrap">
    % stash proc => $proc;
    %= include "_short_proc"
  </div>
% }
</div>

@@ procName.html.ep
% layout "wrapper", title => "RFC:Toy Process Table Viewer, " . $processes->[0]->realfname;
% my $count = @$processes;
<h2><%= $count %> Process<%= "es" unless $count == 1 %>
    Named <span style="color:#900"><%= $processes->[0]->realfname %></span></h2>
% stash procs => $processes, fields => $fields;
%= include "_long_proc"

@@ procNamepid.html.ep
% layout "wrapper", title => "RFC:Toy Process Table Viewer, [" . $proc->pid . "] " . $proc->realfname;
% my $endpoint = $c->match->endpoint->name;
<h2><%= "[" . $proc->pid . "] " if $endpoint eq "procNamepid" %>
    <a href="<%= $c->url_for("procName", procName => $proc->realfname) %>"><%= $proc->realfname %></a></h2>
% stash procs => [ $proc ], fields => $fields;
%= include "_long_proc"

@@ not_found.html.ep
% layout "wrapper", title => "404: Not Found";
<h2>404: Not Found</h2>
% my $message = stash("message") || "No such resource: " . $c->req->url;
<blockquote>
   <%= $message %>
</blockquote>
<pre style="display:none">
%= dumper $c;
</pre>

@@ layouts/wrapper.html.ep
<!DOCTYPE html>
<html>
  <head>
    <title><%= $title || "RFC:Toy Process Table Viewer" %></title>
    <style>
      * { font-family: roboto, "helvetica neue", sans-serif; font-weight: 400 }
      html { margin: 4% auto; max-width: 60em; }
      pre,tt,code { font-family: monaco, monospace }
      a { text-decoration: none }
      a:hover { text-decoration: underline }
      span.pid { display:inline-block; width:5em; text-align:right; cursor:pointer }
      .process { border-radius:.5rem; margin:.5rem 0; padding:.25rem .5rem }
      .process h3, .process h4 { font-family: monaco, monospace; margin:0 0 0.1rem 3rem; text-indent:-2em }
    </style>
  </head>
  <body>
    <h1>
    % if ( $c->endpoint ) {
       <a href="<%= $c->url_for("/") %>">RFC:Toy Process Table Viewer</a>
    % } else {
       RFC:Toy Process Table Viewer
    % }
    </h1>
    <%= content %>
  </body>
</html>
