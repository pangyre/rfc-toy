#!/usr/bin/env perl
use utf8;
use 5.14.2;
use strict;
use Plack::Request;
use Pod::Simple::HTML;
use HTML::Entities;
use URI::Escape;
use URI;
use Capture::Tiny "capture";

my $Name = "RFC:Toy Perl Documentation Viewer";

my @cached_inc;
for my $module ( keys %INC )
{
    next unless my $package = _package_from_path($module);
    # ^^^ Uneccessary, backwards, right?
    next unless capture { system perldoc => -l => $package };
    # ^^^ Only what has POD.
    push @cached_inc, $package;
}

my $Style = <<"";
<style>
    html { margin:4% auto; max-width:60em; }
    body { padding-bottom:3em }
    * { font-family:roboto, "helvetica neue", sans-serif; line-height:130% }
    h1 { font-weight:100 }
    h2, h3, h4 { font-weight:200 }
    p, ul { font-weight:300 }
    pre,tt,code { font-family:monaco, monospace; font-weight:200; font-size:85%; line-height:150% }
    a { text-decoration:none }
    a:hover { text-decoration:underline }
    </style>

sub _template {
    my $h1 = shift;
    my $h2 = shift;
    my @content = @_;
    <<"_HTML_";
<!DOCTYPE html>
<html>
  <head>
    <title>$Name</title>
    $Style
  </head>
  <body>
  <h1>$h1</h1>
  <h2>$h2</h2>
  @{[ join $/, @content ]}
  </body>
</html>
_HTML_
}

sub _after_title {
    my $package = shift;
    my $name = shift;
    <<"";
</title>
    $Style
  </head>
  <body>
    <h1><a href="./">$Name</a></h1>
    <h2>$package</h2>

}

sub _HTML_for_file {
    my $req = shift;
    my $path = shift;
    my $package = shift;
    my $root_uri = URI->new_abs( "/", $req->uri );
    $Pod::Simple::HTML::Perldoc_URL_Prefix = $root_uri;
    $Pod::Simple::HTML::Doctype_decl = "<!DOCTYPE html>\n";
    my $p = Pod::Simple::HTML->new;
    $p->output_string(\my $html);
    $p->title_prefix("$Name, ");
    $p->html_header_after_title( _after_title($package) );
    $p->html_h_level(3);
    $p->parse_file($path);
    $html;
}

sub _package_from_path {
    my $path = shift;
    return unless ( my $package = $path ) =~ s/\.pm\z//;
    $package =~ s{/}{::}g;
    $package;
}

sub {
    my $req = Plack::Request->new(shift);
    my $res = $req->new_response(200, [ "Content-Type" => "text/html" ]);
    my $path_info = $req->path_info;
    my $module = $path_info =~ s{\A/}{}r;
    my $path;
    chomp( ( $path ) = capture { system perldoc => -l => $module } )
        if $module =~ /\A[a-zA-Z:0-9]+\z/;

    if ( $module eq "-" )
    {
        $res->body( _HTML_for_file( $req, __FILE__ ) );
    }
    elsif ( $path )
    {
        my $html = _HTML_for_file( $req, $path, $module )
            || _template(qq{<a href="./">$Name</a>},
                         $module,
                         $module, "has no documentation." );
        $res->body($html);
    }
    elsif ( my $path = $path_info =~ s{\A/}{}r )
    {
        $res->code(404);
        $res->body( _template(qq{<a href="./">$Name</a>},
                              "404: Not Found",
                              "No such package:",
                              encode_entities($module) ) );
    }
    else
    {
        my @body;
        for my $package ( @cached_inc )
        {
            push @body, sprintf qq{<li><a href="./%s">%s</a></li>\n},
                uri_escape($package), $package;
        }
        $res->body( _template( qq{<a href="./-">$Name</a>},
                               "Dependencies for This Toy",
                               "<ol style='columns:2'>", sort(@body), "</ol>") );
    }
    $res->finalize;
};

__END__

=pod

=encoding utf8

=head1 Self-Documentation

PerlDocumentationViewer.plack.psgi - implementation of L<RFC:Toy
{ProgrammingLanguage} Documentation
Viewer|https://bitbucket.org/pangyre/rfc-toy/> using low level L<PSGI>.

=head1 Synopsis

 git clone git@bitbucket.org:pangyre/rfc-toy.git
 plackup rfc-toy/ex/perl/PerlDocumentationViewer.plack.psgi -r

=head1 Requirements

Minimum Perl version is B<5.14.2>.

Required modules: L<Plack::Request>, L<Pod::Simple::HTML>, L<URI>,
L<HTML::Entities>, and L<Capture::Tiny>.

=head1 Notes

This is poor teaching/example code. It's badly abstracted and is a
slightly moronic hybrid approach heavy on idiom resulting from first
draft code. As a solved Toy and first draft, I might keep it as
awkward as it is and do a cleaner one to compare and contrast. The
template code should be reused for every page instead of having it +
half a template approach for the L<POD::Simple::HTML> generated pages.
The POD code should probably be an inlined subclass.

=head1 Code Repository

This toy: L<https://bitbucket.org/pangyre/rfc-toy/src/master/ex/perl/PerlDocumentationViewer.plack.psgi>.

Its specification: L<https://bitbucket.org/pangyre/rfc-toy/src/master/specs/LanguageDocumentationViewer.md>.

=head1 Author and Copyright

©2019 Ashley Pond V.

=head1 License and Disclaimer of Warranty

You may redistribute and modify this package under the same terms as Perl itself.

This software is licensed free of charge. This software has no
warranty, protections, or guarantees whatsoever.

=cut
