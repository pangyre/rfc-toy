# Sample Code and Test Matrix

(Should be automated…) Should each different example get a custom URI
Root? Probably, right? So they can be run together. Tests should strictly
reflect the spec and any compliant toy will pass them.

Toy | Example Code | Port | URI Root | Test | Status
--- | --- | --- | --- | --- | ---
Hello, Toy! | ex/perl/HelloToy.sh | | /hello | HelloToy.t | ✓
Chalk Board | perl/ChalkBoard.plack.psgi | | /chalk | ChalkBoard.t | ✓
Process Table Viewer | perl/ProcTableViewer.mojo.pl | | /proc | ProcTableViewer.t | ✓
Process Table Viewer | perl/ProcCat | | /proc | ProcTableViewer.t | ✓
{Language} Documentation Viewer | perl/PerlDocumentationViewer.plack.psgi | | /pod | LanguageDocumentationViewer.t | ✓
{ToyName} | {ex/path} | | {/uniq} | {testName}.t | ✗

**✓** Spec is reasonably complete, sample code passes test.  
**✗** Missing or failing.
